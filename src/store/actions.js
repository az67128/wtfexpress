import { get } from "svelte/store";
import {
  goods,
  goodsCount,
  view,
  turnState,
  activeTeamIndex,
  activeGoodIndex,
  activeTeam,
  teams,
} from "./index";
import { VIEWS } from "./constants";

export const sendMetrik = (name, payload) => {
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = window.ym || consoleMetrik;
  ym(62545420, "reachGoal", `${name}`, payload);
};

export const startGame = () => {
  sendMetrik("GAME_START", { goodsCount: get(goodsCount) });
  goods.init();
  teams.reset();
  activeGoodIndex.reset();
  view.set(VIEWS.TURN_INTRO);
  turnState.start();
};

export const startTurn = () => {
  view.set(VIEWS.METHOD);
  setTimeout(() => {
    view.set(VIEWS.TURN);
  }, 1500);

  turnState.start();
};

export const guessed = () => {
  get(activeTeam).addScore();
  turnState.win();
};

export const timeIsUp = () => {
  turnState.lose();
};

export const nextTurn = () => {
  activeTeamIndex.next();
  if (get(activeGoodIndex) + 1 === get(goodsCount)) {
    view.set(VIEWS.RESULT);
  } else {
    view.set(VIEWS.TURN_INTRO);
    activeGoodIndex.next();
    turnState.start();
  }
};

export const newGame = () => {
  sendMetrik("NEW_GAME");
  view.set(VIEWS.GAME_SETUP);
};

export const close = () => {
  sendMetrik("GAME_CLOSE");
  view.set(VIEWS.INTRO);
};

export const goodClick = (url) => {
  sendMetrik("GOOD_CLICK", { goodUrl: url });
};

export function parseGood() {
  console.log(
    JSON.stringify({
      url: window.location.href.split("?")[0],
      description: document.querySelector(".product-title").innerText,
      images: [document.querySelector(".magnifier-image").src],
      type: "GESTURE",
    })
  );
}
