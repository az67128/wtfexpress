export default [
  {
    url: "https://aliexpress.ru/item/4000215760064.html",
    images: [
      "https://ae01.alicdn.com/kf/H30a4114c7f6e4a29bfe2dd3023233e12T/40.jpg",
    ],
    description:
      "Высокое Качество Лук Рыболовная катушка рыбалка охота лук 40 м стрельба рыбный горшок стрельба из лука стрела оборудование для наружной рыбалки аксессуары",
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000174771584.html",
    images: [
      "https://ae01.alicdn.com/kf/Hc5f7ba55ed8947aab3e360e7dbcd85f09/1-38-357.jpg",
      "https://ae01.alicdn.com/kf/Hcabfc0391fef4d589d4e74fe1dde3223l/1-38-357.jpg",
    ],
    description:
      "1 шт. скоростная полоса для револьвера подходит. 38 или. 357 Калибр держать 6 патронов пули в поясной сумке Прямая поставка оптовая продажа",
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000601289436.html",
    images: [
      "https://ae01.alicdn.com/kf/Ha288ebaf41584059b073c6fd823935bcz/1-6-M004-Halo-UDT.jpg",
      "https://ae01.alicdn.com/kf/H1e8e0aad8a8246dc93d634cef79a35cfs/1-6-M004-Halo-UDT.jpg",
    ],
    description: `1/6 масштаб M004 США морской котик Halo UDT ночной Дайвинг десантник Джампер модель для 12 "экшн-фигурка весь набор тело Модель Кукла`,
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000509816890.html",
    description:
      "202 шт./компл 3D пузырь форме звезды, которая светится в горошек стены Стикеры детской комнаты Спальня наклейка для домашнего декора светится в темноте DIY Стикеры s наклейки на стену для дома",
    images: [
      "https://ae01.alicdn.com/kf/Hfa7f79746f12465e91990cacd7f2fc64p/202-3D.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32978708091.html",
    description:
      "Кошачий сахарный шар, снеки для кошек, конфетный лижут, твердый питательный Кот, энергетический шар, игрушка с натуральным кошачьим мячом и присоской для кошек",
    images: ["https://ae01.alicdn.com/kf/HTB13ccWJb2pK1RjSZFsq6yNlXXa2/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000583266721.html",
    description:
      "Шерстяной войлочный отпариватель для пельменей, ручная работа, материал, упаковка, игла, не готовая шерсть, войлочные куклы, креативный подарок на день рождения",
    images: [
      "https://ae01.alicdn.com/kf/Hf0d479089ad8464f91dfdefb3f5ac1e5M/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000796091729.html",
    description:
      "Половинная жизнь 2 головы Краб плюшевая игрушка кукла подарок новый",
    images: [
      "https://ae01.alicdn.com/kf/H254ef02dac964abb9070a70d08006dd40/2.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32953593346.html",
    description:
      "2 шт., милая имитация попугая, птица, лиса, поросенок, фигурка бобра, модель животного, домашний декор, миниатюрная фея, декор для сада, аксессуары, фигурка",
    images: ["https://ae01.alicdn.com/kf/HTB1bH3wXUvrK1RjSszfq6xJNVXam/2.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000326360885.html",
    description:
      "3d Шоколадный Бар плесень Поликарбонат формы для выпечки торт конфеты лоток Жесткий ПК Diy Плесень квадратный экологичный силикон силиконовая форма Diy",
    images: ["https://ae01.alicdn.com/kf/HTB1NiF_byLrK1Rjy1zdq6ynnpXaN/3d.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32937277554.html",
    description:
      "Грязевая палочка с антипригарным покрытием, твердая пустотелая домашняя кухня, столовое скольжение, помадка, Скалка, помадка, торт, тесто, ролик, украшение",
    images: ["https://ae01.alicdn.com/kf/HTB1CpomXcfrK1RkSnb4q6xHRFXaS/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33015245438.html",
    description:
      "Лучше распределитель масла из нержавеющей стали-легко распределить холодное твердое масло кухонные аксессуары инструменты для торта нож доска для кухни",
    images: ["https://ae01.alicdn.com/kf/HTB1FpCgVZbpK1RjSZFyq6x_qFXae/-.jpg"],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32919835037.html",
    description:
      "Жевательная форма змеи черви в форме твердых конфет аксессуары для выпечки Силиконовые Кухонные гаджеты 1 шт инструменты для украшения торта",
    images: ["https://ae01.alicdn.com/kf/HTB1yhJVKr9YBuNjy0Fgq6AxcXXaR/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32901374426.html",
    description:
      "1 шт. новый безопасный антипригарный твердый деревянный роликовый штифт для выпечки помадки торт кондитерский ролик",
    images: ["https://ae01.alicdn.com/kf/HTB131fSCAOWBuNjSsppq6xPgpXaK/1.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32955106961.html",
    description:
      "Тонкая палочка, чудесный гель для пупка/Крем для тела для похудения, быстрое похудение/сжигание фигуры на магните/сжигание жира на весе 80 г",
    images: ["https://ae01.alicdn.com/kf/HTB1T7PzXNrvK1RjSszeq6yObFXap/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32974647815.html",
    description:
      "500 шт/рулон, хрупкая ручка для отправки с заботой, стикеры, предупреждающие стикеры 51 мм x 76 мм или предупреждающие наклейки",
    images: [
      "https://ae01.alicdn.com/kf/HTB1pCDia5jrK1RjSsplq6xHmVXao/500.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/33046198375.html",
    description:
      "Астронавт окно пузыря сумка для переноски путешествия дышащая космическая капсула прозрачная переноска для домашних животных сумка Собака Кошка Рюкзак",
    images: [
      "https://ae01.alicdn.com/kf/Hb32fe38c842d4989864ab39f5854e3de0/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000773332158.html",
    description:
      "Портативный милый держатель для мобильного телефона с кошкой, планшеты, стол, подставка для автомобиля, крепление на присоске",
    images: [
      "https://ae01.alicdn.com/kf/H78c51c128458456ab07f3ce21320fc25t/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33022026081.html",
    description:
      "Китайский счастливый богатство развевающаяся кошка Золотая развевающаяся рука кошка домашний декор Добро пожаловать развевающаяся скульптура кошки декоративная статуэтка украшение автомобиля",
    images: [
      "https://ae01.alicdn.com/kf/Hfb110d62a1154413b23755abd00d71adM/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32980139193.html",
    description:
      "Ручной попугай Летающий Поводок Питомца птичий канат жгут с крылом для попугая Cockatiel Sun Parakeet Monk длиннохвостый попугай Попугайчик Летающий костюм",
    images: [
      "https://ae01.alicdn.com/kf/H0ed77277371b4bf6bc9aa2978ac685f8z/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33025105331.html",
    description:
      "Одежда с птицами, подгузник для животных, попугай, салфетки, ручная работа, штаны с птицами, милый Летающий костюм для Cockatiel Sun Parakeet Monk Parakeet, мягкая ткань",
    images: ["https://ae01.alicdn.com/kf/HTB1jVwjaEGF3KVjSZFoq6zmpFXaE/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000174644799.html",
    description:
      "Новый питомец собака желатин игрушка со звуком игрушка для прорезывания зубов вампир лицо подарок на Рождество, Хэллоуин новое поступление",
    images: [
      "https://ae01.alicdn.com/kf/Hb785075fb88c40d196f6786d4f921287p/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32992129409.html",
    description:
      "Бумажная модель HD рисунки Половина Жизни 2 научно-фантастический пистолет",
    images: [
      "https://ae01.alicdn.com/kf/H09ef63af9d6c400ca82c608b21122c2c1/HD-2.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000514740450.html",
    description:
      "Современные светодиодные ночники Квантовая лампа модульная Сенсорная лампа Сенсорное освещение LED ночник Магнитный DIY внутреннее украшение",
    images: [
      "https://ae01.alicdn.com/kf/H8d88c7f7ded14b0c9bbdf1ced96ba8858/-.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000051092377.html",
    description:
      "Пивная свежая защита от вредителей, Вращающаяся крышка, содовые банки, брызги, Пыленепроницаемая крышка, герметизация, дефлектор, бутылка, рот, пластиковый лист, крышка",
    images: [
      "https://ae01.alicdn.com/kf/H19d211be8bdb4cb497e28a2bf89c7b0aw/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33050434319.html",
    description:
      "Вакуумная Сода пробка поп Сода крышка бутылки Пробка бар аксессуары инструменты бутылки свежая крышка",
    images: ["https://ae01.alicdn.com/kf/HTB191w0el1D3KVjSZFyq6zuFpXa8/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32855021687.html",
    description:
      "Электронная Машина для домашних животных, электронная машинка для выращивания яиц динозавра, детская игровая машина для мальчиков и девочек",
    images: ["https://ae01.alicdn.com/kf/HTB1Mud_bkxz61VjSZFtq6yDSVXat/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000164094796.html",
    description:
      "Торт Банкомат копилка потянув безопасные украшения Сюрприз подарок на день рождения LKS99",
    images: [
      "https://ae01.alicdn.com/kf/H4cd31e9692b448aba721d32df585008d8/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000190523113.html",
    description:
      "50 шт., высокое качество, креативная чашка для закусок, творческий день рождения, свадьба, гриль для пикника вечерние, еда, напитки, чашки для молока и чая",
    images: [
      "https://ae01.alicdn.com/kf/He90108bb068146dca2a3e2cded2ebf08j/50.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32854366371.html",
    description:
      "Легкий в 2018 году легкий носок помощь набор носок помощник слайдер беременность и травмы инструмент для жизни",
    images: [
      "https://ae01.alicdn.com/kf/HTB1a4YoneuSBuNjy1Xcq6AYjFXag/2018.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32961278756.html",
    description:
      "Икра Roe соус-мейкер молекулярные инструменты для гурманов Икра коробка дуршлаг ситечко Кухонные гаджеты Оборудование для украшения готовки",
    images: [
      "https://ae01.alicdn.com/kf/HTB1V5qeaffsK1RjSszbq6AqBXXaw/Roe.jpg",
    ],
    type: "WORD",
  },

  {
    url: "https://aliexpress.ru/item/4000600918313.html",
    description:
      "300 г, бесплатная доставка, силиконовый чехол для ног, изогнутая или тонкая силиконовая накладка для ног, коврик для ног",
    images: [
      "https://ae01.alicdn.com/kf/H50a7861675cf4576b630963940612ea2s/300.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000194671159.html",
    description:
      "Fit Technic серии пистолет револьвер пистолет может огонь пули набор DIY модель строительные блоки игрушки для детей мальчиков подарок",
    images: [
      "https://ae01.alicdn.com/kf/Hc1bdd3f949544c1a9b6078fd3583aa25k/Fit-Technic.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33015053151.html",
    description:
      "Трудолюбивые дорожные Наклейки для декора дорожная записанная Наклейка Настенная «Слова» водонепроницаемые Съемные Виниловые настенные наклейки для ноутбука G247",
    images: ["https://ae01.alicdn.com/kf/HTB1g8hJVVzqK1RjSZFoq6zfcXXaJ/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32869955634.html",
    description:
      "Вертолет Забавный потяните переключатель панель наклейка постеры виниловые настенные наклейки Parede Декор настенная работа жесткий Автомобиль Забавная наклейка",
    images: ["https://ae01.alicdn.com/kf/HTB12JVzFuySBuNjy1zdq6xPxFXaF/-.jpg"],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32884641508.html",
    description: "Брошь с кальмаром, летающая для детей",
    images: ["https://ae01.alicdn.com/kf/HTB1TJq4xpOWBuNjy0Fiq6xFxVXaa/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32964979105.html",
    description:
      "Защитный шлем на солнечной батарее, работающий на солнечных батареях, охлаждающий вентилятор на рабочем месте",
    images: ["https://ae01.alicdn.com/kf/HTB1Y2sbacfrK1RkSmLyq6xGApXaz/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32842363447.html",
    description:
      "Плоскогубцы для носа из нержавеющей стали, инструмент для тяги",
    images: ["https://ae01.alicdn.com/kf/HTB14b0YctfJ8KJjy0Feq6xKEXXa3/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32864210862.html",
    description:
      "1:1 MG42 Тяжелая машина, пистолет во время Второй мировой войны, немецкий общий пистолет, 3D бумажная модель",
    images: [
      "https://ae01.alicdn.com/kf/H9a6c6216709145cf9f1b09fe64e6442dY/1-1-MG42.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32992141196.html",
    description:
      "Бумажная модель, пересекающая пожарную леску Cs1: 1, пистолет Ak74, ружье, 3D бумажная модель, ручная работа, сделай сам",
    images: [
      "https://ae01.alicdn.com/kf/Ha9b9953b6eae4a7c80355b8b0ce5b19d6/Cs1-1.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000166203698.html",
    description:
      "1:1 120 см Реактивная граната DIY 3D бумажная карточка модель Конструкторы строительные игрушки развивающие игрушки Военная Модель",
    images: [
      "https://ae01.alicdn.com/kf/H45bfae87abc64f21bc8bfc50eb8bfd1eX/1-1-120-DIY-3D.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000552007771.html",
    description:
      "35 см забавные шапки Пикачу, мягкие плюшевые игрушки в виде животных из мультфильмов, креативная шляпа, кукла, милые вечерние игрушки для детей, девочек, подарки на день рождения, Рождество",
    images: [
      "https://ae01.alicdn.com/kf/H6793b80c8157498bac0e5461f50ac745T/35.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000468554418.html",
    description:
      "Мощный сплав рогатки лук катапульты с лазером высокой точности съемки для охоты съемки рыбалки складной наручный слинг выстрел",
    images: [
      "https://ae01.alicdn.com/kf/H5ab13bbebade4e25a0f4736188779495H/-.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000426562017.html",
    description:
      "Flytec V002 радиоуправляемая лодка 2,4G с дистанционным управлением, электрическая гоночная лодка для бассейнов с имитацией головы крокодила, игрушки для детей и взрослых",
    images: [
      "https://ae01.alicdn.com/kf/H4a4ae218412b4382bc283fc44080e34aJ/Flytec-V002-2-4G.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32850864598.html",
    description:
      "Power Bird игрушка резиновая лента будет лететь птица фестиваль подарок 2020",
    images: [
      "https://ae01.alicdn.com/kf/HTB1U3BrosLJ8KJjy0Fnq6AFDpXa6/Power-Bird.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000249974104.html",
    description:
      "Открытый газон пляж море животное надувной водный спрей дети разбрызгиватель игровой коврик Ванна Плавательный Бассейн",
    images: [
      "https://ae01.alicdn.com/kf/H5fea7affa0e540dd888d63d10e216bb8y/-.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000437031997.html",
    description:
      "Моделирование Q1 буксир яйцо лодка Мини Q лодка маленький росток буксир спасательная лодка модель дистанционного управления набор Материал 1:18",
    images: [
      "https://ae01.alicdn.com/kf/Ha34b0a303ae94e279ba0108ed9d61324D/Q1-Q.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32974484046.html",
    description:
      "Тактическая игрушечная водяная пуля, пластиковая коробка для хранения чемоданов Glock G18/G17/M92/1911 и других маленьких Blaster",
    images: ["https://ae01.alicdn.com/kf/HTB1JEj4biLrK1Rjy1zdq6ynnpXaM/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000239636553.html",
    description:
      "1 шт. покеболы + 1 бесплатная случайная игрушка фигурка игрушка Пикачу Кукла Горячая",
    images: [
      "https://ae01.alicdn.com/kf/H04fb76b6e9114ecb9b43e792679cf96ep/1-1.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32867418923.html",
    description:
      "Мягкие плюшевые игрушки Alien Chestburster/Лицевая груша, Классические игрушки для фильмов и ТВ",
    images: [
      "https://ae01.alicdn.com/kf/HTB1THLUSpzqK1RjSZFCq6zbxVXa0/Alien-Chestburster.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000760417529.html",
    description:
      "Лазерный набор рыболовных рогаток из нержавеющей стали с инфракрасным прицеливанием для охоты на открытом воздухе, можно использовать стальные шарики и стрелы",
    images: [
      "https://ae01.alicdn.com/kf/He8045763e7944b179853648f07e39badX/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000820900999.html",
    description:
      "Детские влажные салфетки термостат Портативный Влажный нагревательный ящик бытовой полотенцесушитель теплоизоляция для ребенка",
    images: [
      "https://ae01.alicdn.com/kf/Ha45b9c5495804880aa779e69d55b09d8b/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000211378681.html",
    description:
      "Смешанная шерстяная руда раскопки открыть минеральные шахты развивающие игрушки обучение и образование история",
    images: [
      "https://ae01.alicdn.com/kf/H58fc7043f9b1413ebd1bf142d2051d34G/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000851908608.html",
    description:
      "100 г высокоактивные мгновенные сухие дрожжи для булочек для хлеба кухня Выпечка Сделай Сам порошок поставки бытовой",
    images: [
      "https://ae01.alicdn.com/kf/Ha05ef41ad76441d887a1f59938edd517s/100.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000864716886.html",
    description:
      "Saizhibaba Сделай Сам деревянная пищеварительная система головоломка развивающая интеллектуальная игрушка модель игрушки Стволовые Обучающие Развивающие игрушки подарок",
    images: [
      "https://ae01.alicdn.com/kf/H7b17cde231bd4eaca39057886674314a8/Saizhibaba.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000669740197.html",
    description:
      "Спящий дом анти-ролл Плоская Голова Регулируемая безопасная спальня малыша младенцы из смеси хлопка детский подушка",
    images: [
      "https://ae01.alicdn.com/kf/H4c21ef5befe241a29a6e928b2818b97eE/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33046115995.html",
    description:
      "Профессиональная 1080P WIFI камера 2MP солнечные панели дикая охотничья камера с инфракрасным светодиодом PIR водонепроницаемая ночного видения SD карта",
    images: [
      "https://ae01.alicdn.com/kf/HTB1miOAdCWD3KVjSZSgq6ACxVXaH/1080P-WIFI-2MP.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000610978748.html",
    description:
      "Новинка; одежда для кошек с героями мультфильмов; милая креативная модная одежда; костюм для питомцев; Одежда для кошек; пальто; Vetement Chat; аксессуары для питомцев; YY50CT",
    images: [
      "https://ae01.alicdn.com/kf/H10ed1fb24f7540f182e6880eb545c4ebG/-.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000046822906.html",
    description:
      "Складное подвесное сиденье в автомобиль зонт крышка водонепроницаемый полиэстер, сумка для хранения автомобиля Органайзер чехол",
    images: [
      "https://ae01.alicdn.com/kf/H68a3c1feebdc44b78f063bcb8ca4fa2cW/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32846397512.html",
    description:
      'Группа поддержки Профессиональный Pom pom двойной головкой представление косплей 6 "18 штук',
    images: [
      "https://ae01.alicdn.com/kf/HTB1.eijc7fb_uJjSsD4q6yqiFXaw/Pom-pom.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32860933663.html",
    description:
      "DXBQYYXGS Электрическая мухобойка убийца комаров Ультразвуковой Отпугиватель комаров Ловушка светодиодный мухобойка лампа защиты от вредителей Отклонить управления DC12V",
    images: [
      "https://ae01.alicdn.com/kf/HTB1q6J2ajDuK1Rjy1zjq6zraFXa7/DXBQYYXGS.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000459385188.html",
    description:
      "Новая FMA Половина маска тюленя для тактического шлема Аксессуары для наружного армейского шлема охотничья страйкбольная Складная маска",
    images: [
      "https://ae01.alicdn.com/kf/Hec35dead655744a3853f38ddf3c09e76i/FMA.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000539655878.html",
    description:
      "3D сотовый мобильный телефон HD экран усилитель лупа для телефона фильмы смартфон кронштейн держатели дети взрослые подарки",
    images: [
      "https://ae01.alicdn.com/kf/H06931f7797104524b20134df41afdff3M/3D-HD.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33030592583.html",
    description:
      "Pokich 1 шт. многоразовые складные Висячие ловушки летних комаров Ловушка ловушка муха ОСА насекомых контроль сада дома двора поставки",
    images: [
      "https://ae01.alicdn.com/kf/HTB1cFKDbEGF3KVjSZFmq6zqPXXaj/Pokich-1.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32964934824.html",
    description:
      "10 шт., Мягкая Силиконовая зубная щетка для домашних животных, очиститель зубов, для собак, кошек, чистящие принадлежности для зубов, оптовая продажа",
    images: [
      "https://ae01.alicdn.com/kf/H4beb8d9af05f45c9920b78cc11146b04G/10.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32918774274.html",
    description:
      "2200 в электрическая ракетка для насекомых Swatter Zapper USB 1200 мАч перезаряжаемая Москитная Swatter Kill Fly 3 сетевая ловушка для насекомых Zapper",
    images: [
      "https://ae01.alicdn.com/kf/H651db08fc78e4a87bf2dbbb6d1fd0738o/2200-Swatter-Zapper-USB-1200.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000715006411.html",
    description:
      "Детские блестящие игрушки на открытом воздухе, Балансирующий велосипед, От 2 до 6 лет, алюминиевый сплав, Сверхлегкий, безопасный, спортивный, для детей, горка, велосипед, подарок",
    images: [
      "https://ae01.alicdn.com/kf/Hb2f0826a5a8c49e09c2d95c08ddb2eb7L/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000720525333.html",
    description:
      "Горячие колеса Кобра гонка туннель взволнованный автомобиль Гонки Приключения соревнование Игрушка Мальчики Набор рельсов детские игрушки Oyuncak Araba FNB20 FNB21",
    images: [
      "https://ae01.alicdn.com/kf/H70a028f8fa584fedbb148344dec85d5dZ/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000723332971.html",
    description:
      "Магнитная Таблица времени календарь удерживающие магниты белая доска Таблица наград за поведение 40 шт. магнитная карта для работы Дети Развивающие игрушки",
    images: [
      "https://ae01.alicdn.com/kf/Hb9be219541f14e58a3b5cfcf607fce0cy/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32817060685.html",
    description:
      "Vanzlife щетка для мытья туалета, ванной комнаты, изогнутая щетка для чистки окон, жалюзи, кухонные аксессуары, Волшебная щетка для очистки пыли",
    images: [
      "https://ae01.alicdn.com/kf/Hb5248c458f37460c95f6d08b2a6af2acj/Vanzlife.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32890043410.html",
    description:
      "робот пылесос робот мойщик окон робот для мытья окон мойка окон робот робот для мойки окон робот для окон",
    images: [
      "https://ae01.alicdn.com/kf/H18d447427870496f82e9fd6137ca1a34d/-.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000601616248.html",
    description:
      "Медовый блонд кудрявый парик на кружеве Омбре Выделите цветной 13x6 парик на кружеве человеческих волос для женщин боб парик 360 фронта шнурка al парик",
    images: [
      "https://ae01.alicdn.com/kf/H27c56aac202049dfbd695e09623ffeacV/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33055193239.html",
    description:
      "Горячая кошка коврик акула форма дом теплый питомник котята кровать один коврик Два использования питомник кошки кровати открытый тент товары для питомцев корзина для кошек",
    images: [
      "https://ae01.alicdn.com/kf/Hd905d5285d6e48798a1824a6d147d8edU/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000109469031.html",
    description:
      "20 скоростей мощные большие вибраторы для женщин Волшебная палочка массажер для тела секс-игрушка для женщин для взрослых секс-клитор стимулирующий продукт",
    images: [
      "https://ae01.alicdn.com/kf/He63be17d42024c94b32cda551648155fA/20.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32963626070.html",
    description:
      "5 шт./партия расческа расческа для волос гребень для волос Расческа для бороды расческа симметричная Расческа Салон усы трафарет для бороды для формирования бороды инструмент для обрезки",
    images: [
      "https://ae01.alicdn.com/kf/H81d26bc0a9b745e698301103038d3df86/5.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000197384220.html",
    description:
      "Новое поступление, креативный хлеб для тостов, подушка для кошек, товары для домашних собак, коврик для кровати, Мягкая Подушка плюшевая подушка, подарки на сиденье, коврик для кошек, матрас каврики",
    images: [
      "https://ae01.alicdn.com/kf/Hb61b96cdaf364fbaa61182dabcb4bb777/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000420967433.html",
    description:
      "Pet стул коврик для гамака Регулируемый двухсторонний кошка собака холст подвесная кровать одеяла гнездо товары для домашних животных",
    images: [
      "https://ae01.alicdn.com/kf/Hf34718f7ed6a4a9ebb4f7c02f0d4aed1H/Pet.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/4000601833866.html",
    description:
      "Банан кошка кровать Дом Уютный милый банан щенок Подушка Питомник теплая портативная корзина для домашних животных принадлежности коврик кровати для кошек и котят",
    images: [
      "https://ae01.alicdn.com/kf/H6a2bf5b4f7564ea28300ca9eb7c531333/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32852573652.html",
    description:
      "Светильник для туалета, водонепроницаемый с датчиком движения, 8 цветов",
    images: ["https://ae01.alicdn.com/kf/HLB1UsNxX0fvK1RjSspoq6zfNpXaw/-.jpg"],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32833823480.html",
    description:
      "Лучшее качество 3D Карта мира Космос галактика полиэстер/хлопок настенный гобелен домашний декор для гостиной космический цветочный пляжный коврик",
    images: ["https://ae01.alicdn.com/kf/HTB1_fGMim3PL1JjSZFxq6ABBVXa9/3D.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32970425278.html",
    description:
      "Делюкс стирание карта мира путешествия царапины для карты 82,5x59,4 см комнаты дома офиса украшения наклейки на стену",
    images: ["https://ae01.alicdn.com/kf/HLB1bw8fa2fsK1RjSszbq6AqBXXaY/82.jpg"],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32884006668.html",
    description:
      "Стол Ньютон колыбель Баланс стальные шары настольные игры Раннее детство образование игрушки Физика Наука Маятник настольные игры",
    images: [
      "https://ae01.alicdn.com/kf/Hd71a0e042ba642f999b102a7effa825dY/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32899050069.html",
    description:
      '3 цвета 30-50lbs F179 Рекурсивный лук 56 "Американский охотничий раздельный бант с DIY аксессуарами для охоты на открытом воздухе для начинающего использования',
    images: [
      "https://ae01.alicdn.com/kf/H3b78a19fa7e54a9abc8344e050ca1748f/3-30-50lbs-F179-56.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32815050143.html",
    description:
      'Позвоночник 500 углеродная стрела 28 "/30" Длина со сменными наконечниками стрел и регулируемыми носками для соединения/изогнутого лука стрельбы из лука',
    images: [
      "https://ae01.alicdn.com/kf/HTB14gxYndfJ8KJjy0Feq6xKEXXah/500-28-30.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32961325885.html",
    description:
      "WORTHBUY 6 шт./партия силиконовый чехол для еды универсальные силиконовые крышки для кухонной посуды чаша горшок многоразовые эластичные кухонные крышки аксессуары",
    images: [
      "https://ae01.alicdn.com/kf/H9850cb2e584d4cf8aa876f72bdc60698m/WORTHBUY-6.jpg",
    ],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32844709019.html",
    description:
      "Новый мультфильм красота и чудовище чайник кружка Mrs Potts Чип чайник чашка один набор прекрасный Рождественский подарок Быстрая доставка",
    images: [
      "https://ae01.alicdn.com/kf/HTB1YXjocjgy_uJjSZR0q6yK5pXaj/Mrs-Potts.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32894305399.html",
    description:
      "4 шт./компл. зажим для листов простыни для кровати с креплением на ремень матрас Эластичный Нескользящий зажим одеяло захват белый и черный",
    images: ["https://ae01.alicdn.com/kf/HTB1c1W3DY1YBuNjSszeq6yblFXaa/4.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/33026763180.html",
    description:
      "WORTHBUY 4 шт./компл., милый котел для яиц, пластиковый набор для яиц, кухонная плита, инструменты для яиц, форма для яиц с крышкой, щетка для блинов",
    images: [
      "https://ae01.alicdn.com/kf/HTB1aShecvWG3KVjSZFPq6xaiXXaG/WORTHBUY-4.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32820884238.html",
    description:
      "S-noilite 20 дюймов невидимый провод без заколки для волос Расширения секретная Рыбная линия шиньоны шелковистые прямые настоящие натуральные синтетические",
    images: [
      "https://ae01.alicdn.com/kf/H845e9f5fcc3d42e08198de99462fc2f6C/S-noilite-20.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32846169489.html",
    description:
      "WORTHBUY китайский 304 горячий горшок из нержавеющей стали 28/30/32 см кухонный суп стоковый горшок кухонная посуда для индукционных кухонных принадлежностей",
    images: [
      "https://ae01.alicdn.com/kf/HTB16bu_kcbI8KJjy1zdq6ze1VXaS/WORTHBUY-304-28-30-32.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000258756070.html",
    description:
      "Кастрюля для завтрака с семью отверстиями многофункциональная колесная Сковорода для блинов маленькая сковорода для яиц Клецка антипригарная сковорода форма для жарки яиц",
    images: [
      "https://ae01.alicdn.com/kf/H214c788689784d40bc198e8104116391U/-.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32895158996.html",
    description:
      "Головная повязка на голову с Микки и Минни Маус, для вечеринки, детский подарок",
    images: ["https://ae01.alicdn.com/kf/HTB1XcXoDTlYBeNjSszcq6zwhFXa1/-.jpg"],
    type: "GESTURE",
  },
  {
    url: "https://aliexpress.ru/item/32398634240.html",
    description:
      "Bamoer аутентичные 925 100% твердых навсегда в форме сердца любовь палец кольцо оригинальный годовщина ювелирные изделия PA7108",
    images: [
      "https://ae01.alicdn.com/kf/HTB1hiUKuFGWBuNjy0Fbq6z4sXXa0/Bamoer-925-100.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000002437961.html",
    description:
      "Фламенко надувной гигантский заказ для детей и взрослых. Коврик для воды большого размера. Спорт, Вытяните Лучшие фотографии.",
    images: ["https://ae01.alicdn.com/kf/HTB1MMcsXNn1gK0jSZKPq6xvUXXa8/-.jpg"],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32892283824.html",
    description:
      "Двери горизонтальные стержни стали 200 кг регулируемые дома тренажерный зал тренировки подбородок пуш-ап выдвижной тренировочный Бар Спорт Фитнес сидения оборудования",
    images: [
      "https://ae01.alicdn.com/kf/H2f5836b6f5204993a7a1a094cf3b074cq/200.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/32974471579.html",
    description:
      "AUGIENB вакуумная Медленная Плита Sous Vide 800 Вт мощная 15л погружная циркуляционная машина с ЖК-цифровым таймером из нержавеющей стали",
    images: [
      "https://ae01.alicdn.com/kf/HTB1bjN0XI_vK1Rjy0Foq6xIxVXa9/AUGIENB-Sous-Vide-800-15.jpg",
    ],
    type: "WORD",
  },
  {
    url: "https://aliexpress.ru/item/4000793279732.html",
    description:
      "24,8 в Nanmu Studio 1/35 Mosasaurus Оригинальная фигурка Властелин бездны динозавр коллектор животные взрослые игрушки подарок предзаказ статуя",
    images: [
      "https://ae01.alicdn.com/kf/H32effc29572c410a817d32429b066252R/24-8-Nanmu-Studio-1-35-Mosasaurus.jpg",
    ],
    type: "WORD",
  },
];
