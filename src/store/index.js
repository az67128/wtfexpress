import { writable, get, derived } from "svelte/store";
import GOODS from "./goods";
import { VIEWS, TEAM_NAMES, TURN_STATE } from "./constants";

let playedGood = 0;

const image = (url = "") => {
  const imageElement = typeof window === "undefined" ? {} : new Image();
  imageElement.crossOrigin = "anonymous";
  if (url) imageElement.src = url;
  const { subscribe, update } = writable({
    url,
    base64: "",
    isLoaded: false,
    error: false,
  });

  imageElement.onload = () => {
    const canvas = document.createElement("CANVAS");
    const ctx = canvas.getContext("2d");
    canvas.height = imageElement.height;
    canvas.width = imageElement.width;
    ctx.drawImage(imageElement, 0, 0);
    const base64 = canvas.toDataURL();
    update((state) => ({ ...state, base64, isLoaded: true }));
  };

  imageElement.onerror = () =>
    update((state) => ({ ...state, isLoaded: false, error: true }));

  return {
    subscribe,
    setUrl: (url) => {
      update((state) => ({ ...state, url }));
      imageElement.src = url;
    },
  };
};

let goodsArray = GOODS.sort(() => 0.5 - Math.random()).map((item, i) => {
  return {
    ...item,
    image: image(i < 10 ? item.images[0] : ""),
  };
});

export const view = writable(VIEWS.INTRO);
export const goodsCount = writable(15);

export const goods = (() => {
  const { subscribe, set } = writable([]);
  return {
    subscribe,
    init: () => {
      if (playedGood + get(goodsCount) > goodsArray.length) {
        playedGood = 0;
        goodsArray = goodsArray.sort(() => 0.5 - Math.random());
      }
      const gameGoods = goodsArray.slice(
        playedGood,
        playedGood + get(goodsCount)
      );
      playedGood += get(goodsCount);
      gameGoods.forEach((item) => {
        if (!get(item.image).url) item.image.setUrl(item.images[0]);
      });
      set(gameGoods);
    },
  };
})();

const Team = ({ color }) => {
  const getRandomName = () =>
    TEAM_NAMES.sort(() => 0.5 - Math.random()).slice(0, 1);
  const { subscribe, update } = writable({
    name: getRandomName(),
    score: 0,
    color,
  });

  return {
    subscribe,
    changeName: () =>
      update((state) => ({
        ...state,
        name: getRandomName(),
      })),
    reset: () => update((state) => ({ ...state, score: 0 })),
    addScore: () => update((state) => ({ ...state, score: state.score + 1 })),
  };
};

export const teams = (() => {
  const { subscribe, update } = writable([
    Team({ color: "--team1" }),
    Team({ color: "--team2" }),
  ]);
  return {
    subscribe,
    reset: () => {
      update((state) => {
        state.forEach((item) => item.reset());
        return state;
      });
    },
  };
})();

export const activeTeamIndex = (() => {
  const { subscribe, update } = writable(0);
  return {
    subscribe,
    next: () => update((state) => (state === 1 ? 0 : 1)),
  };
})();

export const activeGoodIndex = (() => {
  const { subscribe, update, set } = writable(0);
  return {
    subscribe,
    next: () => update((state) => state + 1),
    reset: () => set(0),
  };
})();

export const activeTeam = derived(
  [teams, activeTeamIndex],
  ([$teams, $activeTeamIndex]) => $teams[$activeTeamIndex]
);

export const good = derived(
  [goods, activeGoodIndex],
  ([$goods, $activeGoodIndex]) => $goods[$activeGoodIndex]
);

export const turnState = (() => {
  const { subscribe, set } = writable(TURN_STATE.START);
  return {
    subscribe,
    start: () => set(TURN_STATE.START),
    win: () => set(TURN_STATE.WIN),
    lose: () => set(TURN_STATE.LOSE),
  };
})();
