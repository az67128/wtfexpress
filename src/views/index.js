import { VIEWS } from "../store/constants";
import Intro from "./Intro.svelte";
import Rules from "./Rules.svelte";
import GameSetup from "./GameSetup.svelte";
import TurnIntro from "./TurnIntro.svelte";
import Turn from "./Turn.svelte";
import Result from "./Result.svelte";
import Method from "./Method.svelte";

export default {
  [VIEWS.INTRO]: Intro,
  [VIEWS.RULES]: Rules,
  [VIEWS.GAME_SETUP]: GameSetup,
  [VIEWS.TURN_INTRO]: TurnIntro,
  [VIEWS.TURN]: Turn,
  [VIEWS.RESULT]: Result,
  [VIEWS.METHOD]: Method,
};
